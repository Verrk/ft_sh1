/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: verrk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/11 15:36:46 by verrk             #+#    #+#             */
/*   Updated: 2015/01/06 13:28:28 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_sh1.h"

void			update_env(t_shell **shell)
{
	char		*pwd;

	pwd = NULL;
	pwd = getcwd(pwd, 0);
	free((*shell)->old_pwd);
	(*shell)->old_pwd = ft_strdup((*shell)->pwd);
	free((*shell)->pwd);
	(*shell)->pwd = pwd;
	ft_setenv("OLDPWD", (*shell)->old_pwd, shell);
	ft_setenv("PWD", pwd, shell);
}

void			ft_chdir(t_shell **shell, char *path, char *arg)
{
	int			fd;

	if (access(path, F_OK) == 0)
	{
		if (access(path, X_OK) == 0)
		{
			chdir(path);
			update_env(shell);
		}
		else
		{
			ft_putstr_fd(arg, 2);
			if ((fd = open(path, O_DIRECTORY)) == -1)
				ft_putendl_fd(": not a directory", 2);
			else
				ft_putendl_fd(": permission denied", 2);
			if (fd > 0)
				close(fd);
		}
	}
	else
	{
		ft_putstr_fd("cd: no such file or directory: ", 2);
		ft_putendl_fd(arg, 2);
	}
}

void			ft_cd(t_shell **shell, char **arg)
{
	char		*path;

	if (ft_strlen_tab(arg) == 1)
		path = ft_strdup((*shell)->home);
	else if (ft_strcmp(arg[1], "-") == 0)
		path = ft_strdup((*shell)->old_pwd);
	else
	{
		if (arg[1][0] == '/')
			path = ft_strdup(arg[1]);
		else
		{
			path = (char *)ft_memalloc(ft_strlen((*shell)->pwd) +
												ft_strlen(arg[1]) + 2);
			ft_strcat(path, (*shell)->pwd);
			ft_strcat(path, "/");
			ft_strcat(path, arg[1]);
		}
	}
	ft_chdir(shell, path, arg[1]);
	free(path);
}
