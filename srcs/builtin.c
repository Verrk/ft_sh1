/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/07 11:32:35 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/06 13:38:32 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_sh1.h"

void			ft_env(t_shell *shell)
{
	int			i;

	i = 0;
	while (shell->env[i])
	{
		ft_putendl(shell->env[i]);
		i++;
	}
}

void			ft_hash(t_shell *shell)
{
	t_hash		*tmp;
	int			bool;

	tmp = shell->hash;
	bool = 1;
	while (tmp)
	{
		if (tmp->time > 0)
		{
			ft_putnbr(tmp->time);
			ft_putchar(' ');
			ft_putendl(tmp->path);
			bool = 0;
		}
		tmp = tmp->next;
	}
	if (bool)
		ft_putendl("hash: hash table empty");
}

void			ft_echo(char **arg)
{
	int			i;
	int			n;

	n = 0;
	if (ft_strcmp(arg[1], "-n") == 0)
		n = 1;
	i = n + 1;
	while (arg[i])
	{
		if (i != n + 1)
			ft_putchar(' ');
		ft_putstr(arg[i]);
		i++;
	}
	if (!n)
		ft_putchar('\n');
}

void			builtin(char **arg, t_shell *shell)
{
	if (ft_strcmp(arg[0], "exit") == 0)
		ft_exit(&shell, &arg);
	else if (ft_strcmp(arg[0], "echo") == 0)
		ft_echo(arg);
	else if (ft_strcmp(arg[0], "cd") == 0)
		ft_cd(&shell, arg);
	else if (ft_strcmp(arg[0], "env") == 0)
		ft_env(shell);
	else if (ft_strcmp(arg[0], "hash") == 0)
		ft_hash(shell);
	else if (ft_strcmp(arg[0], "setenv") == 0)
		ft_checkenv(arg, &shell, 1);
	else if (ft_strcmp(arg[0], "unsetenv") == 0)
		ft_checkenv(arg, &shell, 2);
}
