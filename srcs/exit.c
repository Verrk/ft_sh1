/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/08 16:38:43 by cpestour          #+#    #+#             */
/*   Updated: 2014/10/15 22:10:16 by verrk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_sh1.h"

void			free_env(char ***env)
{
	int			i;

	i = 0;
	while ((*env)[i])
	{
		free((*env)[i]);
		i++;
	}
	free(*env);
}

void			free_hash(t_hash **hash)
{
	t_hash		*tmp;

	while (*hash)
	{
		tmp = *hash;
		*hash = tmp->next;
		free(tmp->name);
		free(tmp->path);
		free(tmp);
	}
}

void			free_builtin(char **builtin)
{
	int			i;

	i = 0;
	while (builtin[i])
	{
		free(builtin[i]);
		i++;
	}
}

void			ft_exit(t_shell **shell, char ***arg)
{
	int			i;

	i = 0;
	free_env(&((*shell)->env));
	free((*shell)->path);
	free((*shell)->pwd);
	free((*shell)->old_pwd);
	free((*shell)->cmd);
	free((*shell)->home);
	free_hash(&((*shell)->hash));
	free_builtin((*shell)->builtin);
	free(*shell);
	i = 0;
	while (i < ft_strlen_tab(*arg))
	{
		free((*arg)[i]);
		i++;
	}
	free(*arg);
	exit(0);
}
