/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/07 07:44:38 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/06 13:44:25 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_sh1.h"

void				init_shell(t_shell **shell, char **env)
{
	int				i;
	char			**tmp;

	i = 0;
	(*shell)->env = (char **)malloc(sizeof(char *) * (ft_strlen_tab(env) + 1));
	while (env[i])
	{
		(*shell)->env[i] = ft_strdup(env[i]);
		i++;
	}
	(*shell)->env[i] = NULL;
	i = 0;
	tmp = (*shell)->env;
	while (tmp[i])
	{
		if (ft_strncmp(tmp[i], "PATH=", 5) == 0)
			(*shell)->path = ft_strdup(&(tmp[i][5]));
		if (ft_strncmp(tmp[i], "PWD=", 4) == 0)
			(*shell)->pwd = ft_strdup(&(tmp[i][4]));
		if (ft_strncmp(tmp[i], "OLDPWD=", 7) == 0)
			(*shell)->old_pwd = ft_strdup(&(tmp[i][7]));
		if (ft_strncmp(tmp[i], "HOME=", 5) == 0)
			(*shell)->home = ft_strdup(&(tmp[i][5]));
		i++;
	}
}

void				init_builtin(t_shell **shell)
{
	(*shell)->builtin[0] = ft_strdup("cd");
	(*shell)->builtin[1] = ft_strdup("env");
	(*shell)->builtin[2] = ft_strdup("setenv");
	(*shell)->builtin[3] = ft_strdup("unsetenv");
	(*shell)->builtin[4] = ft_strdup("hash");
	(*shell)->builtin[5] = ft_strdup("echo");
	(*shell)->builtin[6] = ft_strdup("exit");
	(*shell)->builtin[7] = NULL;
}

void				get_hash(t_shell **shell)
{
	char			**path_table;
	DIR				*dir;
	struct dirent	*d;
	int				i;

	path_table = ft_strsplit((*shell)->path, ':');
	i = 0;
	while (path_table[i])
	{
		if ((dir = opendir(path_table[i])) != NULL)
		{
			while ((d = readdir(dir)) != NULL)
			{
				if (d->d_name[0] != '.')
					push(&((*shell)->hash), d->d_name, path_table[i]);
			}
			closedir(dir);
		}
		free(path_table[i]);
		i++;
	}
	free(path_table);
}

int					main(void)
{
	t_shell			*shell;
	extern char		**environ;

	shell = (t_shell *)malloc(sizeof(t_shell));
	shell->hash = NULL;
	init_shell(&shell, environ);
	init_builtin(&shell);
	get_hash(&shell);
	while (42)
	{
		ft_putstr(shell->pwd);
		ft_putstr("$> ");
		get_next_line(0, &(shell->cmd));
		if (shell->cmd[0])
			ft_sh(shell);
		free(shell->cmd);
		shell->cmd = NULL;
	}
	return (0);
}
