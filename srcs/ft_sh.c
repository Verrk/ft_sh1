/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sh.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/07 08:00:15 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/05 15:43:01 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_sh1.h"

char		*get_path(char *arg, t_hash *hash)
{
	if (access(arg, F_OK) == 0)
	{
		while (hash)
		{
			if (ft_strcmp(hash->path, arg) == 0)
				hash->time++;
			hash = hash->next;
		}
		return (arg);
	}
	else
	{
		while (hash)
		{
			if (ft_strcmp(hash->name, arg) == 0)
			{
				hash->time++;
				return (hash->path);
			}
			hash = hash->next;
		}
		return (NULL);
	}
}

int			isbuiltin(char *arg, t_shell *shell)
{
	int		i;

	i = 0;
	while (shell->builtin[i])
	{
		if (ft_strcmp(arg, shell->builtin[i]) == 0)
			return (1);
		i++;
	}
	return (0);
}

void		ft_fork(char *path, char **arg, t_shell *shell)
{
	pid_t	pid;

	pid = fork();
	if (pid > 0)
		wait(NULL);
	if (pid == 0)
		execve(path, arg, shell->env);
}

void		ft_sh(t_shell *shell)
{
	char	**arg;
	char	*path;
	int		i;

	arg = ft_strsplit_space(shell->cmd);
	if (arg && arg[0])
	{
		i = 0;
		if (isbuiltin(arg[0], shell))
			builtin(arg, shell);
		else if ((path = get_path(arg[0], shell->hash)))
			ft_fork(path, arg, shell);
		else
		{
			ft_putstr_fd("ft_minishell: command not found: ", 2);
			ft_putendl_fd(arg[0], 2);
		}
		while (i < ft_strlen_tab(arg))
			free(arg[i++]);
		free(arg);
	}
	else if (arg)
		free(arg);
}
