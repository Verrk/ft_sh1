/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/08 22:29:24 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/05 16:59:09 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_sh1.h"

void			new_env(t_shell **shell, char *str)
{
	char		**new;
	int			i;

	i = 0;
	new = (char **)malloc(sizeof(char *) * (ft_strlen_tab((*shell)->env) + 2));
	while ((*shell)->env[i])
	{
		new[i] = ft_strdup((*shell)->env[i]);
		i++;
	}
	new[i++] = str;
	new[i] = 0;
	free_env(&((*shell)->env));
	(*shell)->env = new;
}

void			rm_env(t_shell **shell, char **arg, int i)
{
	int			j;
	int			k;
	char		**new;

	new = (char **)malloc(sizeof(char *) * ft_strlen_tab((*shell)->env));
	j = 0;
	k = 0;
	while ((*shell)->env[j])
	{
		if (ft_strncmp((*shell)->env[j], arg[i],
						ft_strlen_e((*shell)->env[j])) != 0 ||
			ft_strlen(arg[i]) != ft_strlen_e((*shell)->env[j]))
			new[k++] = ft_strdup((*shell)->env[j]);
		j++;
	}
	new[k] = 0;
	free_env(&((*shell)->env));
	(*shell)->env = new;
}

void			ft_setenv(char *name, char *value, t_shell **shell)
{
	char		*str;
	int			i;
	int			bool;

	str = (char *)ft_memalloc(ft_strlen(name) + ft_strlen(value) + 2);
	ft_strcat(str, name);
	ft_strcat(str, "=");
	ft_strcat(str, value);
	i = 0;
	bool = 1;
	while ((*shell)->env[i] && bool)
	{
		if (ft_strncmp((*shell)->env[i], name,
						ft_strlen_e((*shell)->env[i])) == 0 &&
			ft_strlen(name) == ft_strlen_e((*shell)->env[i]))
		{
			free((*shell)->env[i]);
			(*shell)->env[i] = str;
			bool = 0;
		}
		i++;
	}
	if (bool)
		new_env(shell, str);
}

void			ft_unsetenv(char **arg, t_shell **shell)
{
	int			i;
	int			j;

	i = 0;
	while (i < ft_strlen_tab(arg))
	{
		j = 0;
		while ((*shell)->env[j])
		{
			if (ft_strncmp((*shell)->env[j], arg[i],
							ft_strlen_e((*shell)->env[j])) == 0 &&
				ft_strlen(arg[i]) == ft_strlen_e((*shell)->env[j]))
			{
				rm_env(shell, arg, i);
				break ;
			}
			j++;
		}
		i++;
	}
}

void			ft_checkenv(char **arg, t_shell **shell, int which)
{
	if (ft_strlen_tab(arg) == 1)
		ft_env(*shell);
	else if (ft_strlen_tab(arg) > 3 && which == 1)
	{
		ft_putstr_fd(arg[0], 2);
		ft_putendl_fd(": Too many arguments.", 2);
	}
	else
	{
		if (which == 1)
		{
			if (ft_strlen_tab(arg) == 2)
				ft_setenv(arg[1], "\0", shell);
			else
				ft_setenv(arg[1], arg[2], shell);
		}
		if (which == 2)
			ft_unsetenv(arg + 1, shell);
	}
}
