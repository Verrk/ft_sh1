/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/07 09:40:21 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/05 15:42:30 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_sh1.h"

t_hash			*new_hash(char *name, char *path)
{
	t_hash		*new;
	char		*path_str;

	new = (t_hash *)malloc(sizeof(t_hash));
	path_str = (char *)malloc(ft_strlen(name) + ft_strlen(path) + 2);
	ft_strcpy(path_str, path);
	ft_strcat(path_str, "/");
	ft_strcat(path_str, name);
	if (new)
	{
		new->name = ft_strdup(name);
		new->path = path_str;
		new->time = 0;
		new->next = NULL;
	}
	return (new);
}

void			push(t_hash **hash_table, char *name, char *path)
{
	t_hash		*new;
	t_hash		*tmp;

	new = new_hash(name, path);
	if (*hash_table == NULL)
		*hash_table = new;
	else
	{
		tmp = *hash_table;
		while (tmp->next && ft_strcmp(name, tmp->next->name) > 0)
			tmp = tmp->next;
		new->next = tmp->next;
		tmp->next = new;
	}
}

int				hash_find(t_hash *hash, char *name)
{
	while (hash)
	{
		if (ft_strcmp(hash->name, name) == 0)
		{
			if (ft_strcmp(name, "cd") == 0)
				return (0);
			if (ft_strcmp(name, "hash") == 0)
				return (0);
			return (1);
		}
		hash = hash->next;
	}
	return (0);
}
