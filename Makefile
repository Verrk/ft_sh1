#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/10/07 07:40:15 by cpestour          #+#    #+#              #
#    Updated: 2014/10/11 15:58:02 by verrk            ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME=ft_minishell1
CFLAGS=-Wall -Werror -Wextra -Ilibft/includes
LDFLAGS=-Llibft -lft
SRC=srcs/main.c srcs/ft_sh.c srcs/hash.c srcs/builtin.c srcs/exit.c srcs/env.c \
srcs/cd.c
OBJ=$(SRC:.c=.o)

all: lib $(NAME)

$(NAME): $(OBJ)
	gcc -o $@ $^ $(LDFLAGS)

%.o: %.c
	gcc -o $@ -c $< $(CFLAGS)

lib:
	make -C libft

clean:
	rm -f $(OBJ) *~ \#* srcs/*~ srcs/\#* includes/*~
	make clean -C libft

fclean: clean
	rm -f $(NAME)
	make fclean -C libft

re: fclean all
