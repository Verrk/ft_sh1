/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sh1.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/07 07:47:12 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/06 13:29:23 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SH1_H
# define FT_SH1_H

# include "libft.h"
# include <dirent.h>
# include <sys/wait.h>
# include <signal.h>
# include <stdio.h>

typedef struct		s_hash
{
	char			*name;
	char			*path;
	int				time;
	struct s_hash	*next;
}					t_hash;

typedef struct		s_shell
{
	char			**env;
	char			*home;
	char			*path;
	char			*pwd;
	char			*old_pwd;
	t_hash			*hash;
	char			*cmd;
	char			*builtin[8];
}					t_shell;

void				ft_sh(t_shell *shell);
void				push(t_hash **hash_table, char *name, char *path);
int					hash_find(t_hash *hash, char *name);
void				builtin(char **arg, t_shell *shell);
void				ft_exit(t_shell **shell, char ***arg);
void				ft_checkenv(char **arg, t_shell **shell, int which);
void				ft_env(t_shell *shell);
void				ft_setenv(char *name, char *value, t_shell **shell);
void				free_env(char ***env);
void				ft_cd(t_shell **shell, char **arg);

#endif
