/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/03 19:19:48 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/03 19:19:51 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **lst, void (*del)(void *, size_t))
{
	if (!lst || !*lst)
		return ;
	if ((*lst)->next)
		ft_lstdel(&(*lst)->next, del);
	ft_lstdelone(lst, del);
}
