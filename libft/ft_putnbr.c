/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 15:01:23 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/05 15:45:17 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_abs(int n)
{
	int		ret;

	ret = (n < 0) ? -n : n;
	return (ret);
}

void		ft_putnbr(int n)
{
	if (n < 0)
		write(1, "-", 1);
	if (n >= 10 || n <= -10)
	{
		ft_putnbr(ft_abs(n / 10));
		ft_putnbr(ft_abs(n % 10));
	}
	else
		ft_putchar(ft_abs(n) + '0');
}
