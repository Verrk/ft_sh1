/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/29 16:13:34 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/03 17:56:26 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strjoin(char const *s1, char const *s2)
{
	int		i;
	int		j;
	char	*new;

	if (s1 && s2)
	{
		new = (char *)malloc(ft_strlen(s1) + ft_strlen(s2) + 1);
		i = 0;
		j = 0;
		while (s1[i])
		{
			new[i] = s1[i];
			i++;
		}
		while (s2[j])
		{
			new[i] = s2[j];
			i++;
			j++;
		}
		new[i] = '\0';
		return (new);
	}
	return (NULL);
}
